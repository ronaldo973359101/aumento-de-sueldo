"use client";
import { Inter } from "next/font/google";
import styles from "./page.module.css";
import {
  Container,
  Card,
  Row,
  Text,
  Button,
  Spacer,
  Modal,
} from "@nextui-org/react";
import { useState } from "react";

export default function Home() {
  const [visible, setVisible] = useState(false);

  const handler = () => {
    setVisible(!visible);
  };
  return (
    <div className={styles.main}>
      <Text
        h1
        size={60}
        css={{
          color: "#4DA0FF",
        }}
        weight="bold"
      >
        Jefe, me aumentaria el sueldo ?
      </Text>
      <Row css={{ justifyContent: "center" }}>
        <Button onPress={handler} auto css={{ backgroundColor: "#4DA0FF" }}>
          <Text
            h6
            size={17}
            css={{
              color: "white",
            }}
            weight="bold"
          >
            SI
          </Text>
        </Button>
        <Spacer></Spacer>
        <Button
          auto
          css={{
            backgroundColor: "#C84040",
            position: "relative",
            "&:hover": {
              left: 120,
              cursor: "pointer",
              pointerEvents: "none",
            },
          }}
        >
          <Text
            h6
            size={17}
            css={{
              color: "white",
            }}
            weight="bold"
          >
            NO
          </Text>
        </Button>
      </Row>
      <Text
        h6
        size={15}
        css={{
          color: "#EBF3FF",
          position: "absolute",
          bottom: 0,
          left: 0,
        }}
      >
        Powered by TumiSoft ®
      </Text>
      <ModalPremium visible={visible} handler={handler}></ModalPremium>
    </div>
  );
}

const ModalPremium = ({ visible, handler }: any) => {
  return (
    <Modal
      css={{
        height: "150px",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "Black",
      }}
      closeButton
      open={visible}
      onClose={handler}
      blur
      aria-labelledby="modal-title"
    >
      <Text css={{ color: "white" }} h1>
        Gracias jefe, se le quiere
      </Text>
    </Modal>
  );
};
